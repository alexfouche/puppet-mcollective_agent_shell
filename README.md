# mcollective_agent_shell version 0.0.2

#### Table of Contents

1. [Overview](#overview)
1. [Usage](#usage)
1. [Configuration](#configuration)

## Overview

Run commands with the local shell

The mcollective_agent_shell module is based on the source from https://github.com/puppetlabs/mcollective-shell-agent.

Available Actions:

  * **kill** - Kill a command by handle
  * **list** - Get a list of all running commands
  * **run** - Run a command
  * **start** - Spawn a command
  * **status** - Get status of managed command

## Usage

You can include this module into your infrastructure as any other module, but as it's designed to work with the [choria mcollective](http://forge.puppet.com/choria/mcollective) module you can configure it via Hiera:

```yaml
mcollective::plugin_classes:
  - mcollective_agent_shell
```

## Configuration

Server and Client configuration can be added via Hiera and managed through tiers in your site Hiera, they will be merged with any included in this module

```yaml
mcollective_agent_shell::config:
   example: value
```

This will be added to both the `client.cfg` and `server.cfg`, you can likewise configure server and client specific settings using `mcollective_agent_shell::client_config` and `mcollective_agent_shell::server_config`.

These settings will be added to the `/etc/puppetlabs/mcollective/plugin.d/` directory in individual files.

For a full list of possible configuration settings see the module [source repository documentation](https://github.com/puppetlabs/mcollective-shell-agent).

## Data Reference

  * `mcollective_agent_shell::gem_dependencies` - Deep Merged Hash of gem name and version this module depends on
  * `mcollective_agent_shell::manage_gem_dependencies` - disable managing of gem dependencies
  * `mcollective_agent_shell::package_dependencies` - Deep Merged Hash of package name and version this module depends on
  * `mcollective_agent_shell::manage_package_dependencies` - disable managing of packages dependencies
  * `mcollective_agent_shell::class_dependencies` - Array of classes to include when installing this module
  * `mcollective_agent_shell::package_dependencies` - disable managing of class dependencies
  * `mcollective_agent_shell::config` - Deep Merged Hash of common config items for this module
  * `mcollective_agent_shell::server_config` - Deep Merged Hash of config items specific to managed nodes
  * `mcollective_agent_shell::client_config` - Deep Merged Hash of config items specific to client nodes
  * `mcollective_agent_shell::policy_default` - `allow` or `deny`
  * `mcollective_agent_shell::policies` - List of `actionpolicy` policies to deploy with an agent
  * `mcollective_agent_shell::client` - installs client files when true - defaults to `$mcollective::client`
  * `mcollective_agent_shell::server` - installs server files when true - defaults to `$mcollective::server`
  * `mcollective_agent_shell::ensure` - `present` or `absent`
